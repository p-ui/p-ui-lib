/**
 *
 */
export class PLabel extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this.__onLabelClick = this._onLabelClick.bind(this);
    return self;
  }

  /**
   *
   */
  connectedCallback() {
    this.addEventListener('click', this.__onLabelClick);
  }

  /**
   *
   */
  disconnectedCallback() {
    this.removeEventListener('click', this.__onLabelClick);
  }

  /**
   *
   */
  static get observedAttributes() {
    return ['for'];
  }

  /**
   * @param {labelFor} labelFor
   */
  set labelFor(labelFor) {
    if (this._labelFor) {
      this.removeEventListener('invalid', this.__onLabelClick);
    }
    this._labelFor = labelFor;
    this.addEventListener('invalid', this.__onLabelClick);
  }

  /**
   *
   */
  get labelFor() {
    return this._labelFor;
  }

  /**
   * @param {name} name
   * @param {oldValue} oldValue
   * @param {newValue} newValue
   */
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'for') {
      this.labelFor = newValue;
    }
  }

  /**
   * @param {e} e event
   */
  _onLabelClick(e) {
    let forElement = document.getElementById(this.labelFor);
    if(forElement && typeof forElement.focus === 'function') {
      forElement.focus();
    }
  }

}

customElements.define('p-label', PLabel);
