import css from './p-dialog.css';

/**
 *
 */
export class PDialog extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this._css = css;
    this.attachShadow({mode: 'open'});
    this.__onCloseButtonClick = this._onCloseButtonClick.bind(this);
    this.setAttributeNode(document.createAttribute('hidden'));
    return self;
  }

  /**
   *
   */
  connectedCallback() {
    this.shadowRoot.innerHTML = '<div class="dialog">' +
      '<div class="dialog--title">' +
      '<slot name="title"></slot></div>' +
      '<div class="dialog--body"><slot name="body"></slot></div>' +
      '</div>';
  }

  /**
   *
   */
  disconnectedCallback() {

  }

  /**
   *
   */
  show() {
    this.backdrop = document.createElement('div');
    this.backdrop.classList.add('p-dialog-backdrop');

    this.dlg = document.createElement('div');
    this.dlg.classList.add('p-dialog');
    this.dlg.innerHTML = '<style>' + this._css + '</style>' +
      '<div class="p-dialog--content">' +
      '<div class="p-dialog--title"></div>' +
      '<a href="#" class="p-dialog--close" tabindex="0"' +
      ' onclick="return false;">'+
      `<svg version="1.1" x="0px" y="0px" viewBox="0 0 475.2 475.2"
        class="close-vector" xml:space="preserve">
      <g>
          <path class="close-vector--circle"
            d="M405.6,69.6C360.7,24.7,301.1,0,237.6,0s-123.1,24.7-168,
            69.6S0,174.1,0,237.6s24.7,123.1,69.6,168s104.5,69.6,168,69.6
            s123.1-24.7,168-69.6s69.6-104.5,69.6-168S450.5,114.5,405.6,69.6z
            M386.5,386.5c-39.8,39.8-92.7,61.7-148.9,61.7
            s-109.1-21.9-148.9-61.7c-82.1-82.1-82.1-215.7,0-297.8C128.5,48.9,
            181.4,27,237.6,27s109.1,21.9,148.9,61.7 C468.6,170.8,468.6,
            304.4,386.5,386.5z"/>
          <path class="close-vector--cross"
            d="M342.3,132.9c-5.3-5.3-13.8-5.3-19.1,
            0l-85.6,85.6L152,132.9c-5.3-5.3-13.8-5.3-19.1,0c-5.3,5.3-5.3,13.8,
            0,19.1 l85.6,85.6l-85.6,85.6c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,
            4,9.5,4s6.9-1.3,9.5-4l85.6-85.6l85.6,85.6c2.6,2.6,6.1,4,9.5,4
            c3.5,0,6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,
            0-19.1l-85.4-85.6l85.6-85.6C347.6,146.7,347.6,138.2,342.3,132.9z"/>
      </g>
      </svg>`+
      '</a>' +
      '<div class="p-dialog--body" tabindex="0"></div>' +
      '</div>';

    this.currentTitle = this.querySelector('[slot="title"]');
    this.currentBody= this.querySelector('[slot="body"]');

    this.dlg.querySelector('.p-dialog--title').appendChild(this.currentTitle);
    this.dlg.querySelector('.p-dialog--body').appendChild(this.currentBody);

    this.closeButton = this.dlg.querySelector('.p-dialog--close');

    this.closeButton.addEventListener('click', this.__onCloseButtonClick);
    document.body.appendChild(this.backdrop);
    document.body.appendChild(this.dlg);

    let dialogHeight = this.dlg.querySelector('.p-dialog--content')
      .clientHeight;
    let windowHeight = Math.max(document.documentElement.clientHeight,
      window.innerHeight || 0);

    if(dialogHeight < windowHeight) {
      this.dlg.querySelector('.p-dialog--content')
        .classList.add('p-dialog--content__centered');
    }

    this.dlg.querySelector('.p-dialog--content').style.opacity = null;
    this.dlg.querySelector('.p-dialog--body').focus();
  }

  /**
   * @param {e} e event
   */
  _onCloseButtonClick(e) {
    this.hide();
  }

  /**
   *
   */
  hide() {
    if(this.dlg.parentNode) {
      this.appendChild(this.currentTitle);
      this.appendChild(this.currentBody);
      this.dlg.parentNode.removeChild(this.dlg);
      this.backdrop.parentNode.removeChild(this.backdrop);
      this.closeButton.removeEventListener('click', this.__onCloseButtonClick);
    }
  }

}

customElements.define('p-dialog', PDialog);
