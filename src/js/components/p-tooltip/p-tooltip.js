import css from './p-tooltip.css';
/**
 *
 */
export class PTooltip extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this._css = css;
    this.attachShadow({mode: 'open'});
    this.__onElementAction = this._onElementAction.bind(this);
    this.__onDocumentClick = this._onDocumentClick.bind(this);
    return self;
  }

  /**
   *
   */
  connectedCallback() {
    this.tooltipContainerElement = document.createElement('div');
    this.tooltipContainerElement.classList.add('tooltip--container');
    this.tooltipContentElement = document.createElement('div');
    this.tooltipContentElement.innerHTML = '<style>' + this._css + '</style>';
    this.tooltipContentElement.classList.add('tooltip--content');
    this.tooltipContainerElement.appendChild(this.tooltipContentElement);
    this.tooltipArrowElement = document.createElement('div');
    this.tooltipArrowElement.classList.add('tooltip--arrow');
    this.tooltipContainerElement.appendChild(this.tooltipArrowElement);
    this.shadowRoot.innerHTML = '<slot></slot>';
  }

  /**
   *
   */
  get tooltipFor() {
    return this._tooltipFor;
  }

  /**
   * @param {tooltipFor} tooltipFor
   */
  set tooltipFor(tooltipFor) {
    let elementFor;
    this.setAttribute('hidden', '');
    if (this._tooltipFor) {
      elementFor = document.getElementById(this.tooltipFor);
      if (elementFor) {
        elementFor.removeEventListener('click', this.__onElementAction);
      }
    }
    this._tooltipFor = tooltipFor;
    elementFor = document.getElementById(this.tooltipFor);
    if (elementFor) {
      elementFor.addEventListener('click', this.__onElementAction);
    }
  }

  /**
   *
   */
  static get observedAttributes() {
    return ['tooltip-for'];
  }

  /**
   * @param {name} name
   * @param {oldValue} oldValue
   * @param {newValue} newValue
   */
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'tooltip-for') {
      this.tooltipFor = newValue;
    }
  }

  /**
   * @param {e} e event
   */
  _onElementAction(e) {
    // let x = this.tooltipContainerElement;


    if (this.tooltipContainerElement) {
      let pos = this._elementPosition(e.target);

      while (this.childNodes.length > 0) {
        this.tooltipContentElement.appendChild(this.childNodes[0]);
      }
      this.tooltipContainerElement.style.opacity = 0;
      document.body.appendChild(this.tooltipContainerElement);

      this.tooltipContainerElement.style.transform = 'translateY(-' +
        this.tooltipContainerElement.getBoundingClientRect().height + 'px)';
      this.tooltipContainerElement.style.left = (pos.left +
        (e.target.getBoundingClientRect().width / 2)) + 'px';
      this.tooltipContainerElement.style.top = pos.top + 'px';
      this.tooltipContainerElement.style.opacity = null;
      window.document.addEventListener('click', this.__onDocumentClick);

      document.body.appendChild(this.tooltipContainerElement);
    }
  }

  /**
   * @param {e} e event
   */
  _onDocumentClick(e) {
    if (e.target !== document.getElementById(this.tooltipFor)) {
      this.tooltipContainerElement.style.opacity = 0;
      while (this.tooltipContentElement.childNodes.length > 0) {
        this.appendChild(this.tooltipContentElement.childNodes[0]);
      }
      if (this.tooltipContainerElement.parentNode) {
        this.tooltipContainerElement.parentNode.
          removeChild(this.tooltipContainerElement);
      }
      window.document.removeEventListener('click', this.__onDocumentClick);
    }
  }

  /**
   * @param {obj} obj element object
   * @return {object}
   */
  _elementPosition(obj) {
    let curleft = 0;
    let curtop = 0;
    if (obj.offsetParent) {
      do {
        curleft += obj.offsetLeft;
        curtop += obj.offsetTop;
      } while (obj = obj.offsetParent);
    }
    return {
      left: curleft,
      top: curtop,
    };
  }

}

customElements.define('p-tooltip', PTooltip);
