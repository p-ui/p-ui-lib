/**
 *
 */
export class PForm extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this.__onFormElementSubmit = this._onFormElementSubmit.bind(this);
    return self;
  }

  /**
   *
   */
  connectedCallback() {
    this.form = this.closest('form');
    if(this.form) {
      this.form.setAttribute('novalidate', 'novalidate');
      this.form.addEventListener('submit', this.__onFormElementSubmit);
    }
  }

  /**
   *
   */
  disconnectedCallback() {
    this.form.removeEventListener('submit', this.__onFormElementSubmit);
  }

  /**
   *
   */
  submit() {
    // console.log('submit');
  }

  /**
   * @param {e} e event
   */
  _onFormElementSubmit(e) {
    e.preventDefault();
    e.stopPropagation();
    this.submit();
  }

}

customElements.define('p-form', PForm);
