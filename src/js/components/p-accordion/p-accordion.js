import css from './p-accordion.css';

/**
 *
 */
export class PAccordion extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this._css = css;
    this.attachShadow({mode: 'open'});
    return self;
  }

  /**
   *
   */
  connectedCallback() {
    this.shadowRoot.innerHTML = '<style>' + this._css + '</style>' +
        '<div class="accordion-container"><slot id="slot"></slot></div>';
  }

}

customElements.define('p-accordion', PAccordion);
