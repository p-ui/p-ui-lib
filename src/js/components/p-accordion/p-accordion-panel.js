import css from './p-accordion-panel.css';

/**
 *
 */
export class PAccordionPanel extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this._css = css;
    this.attachShadow({mode: 'open'});
    this.__onTitleClick = this._onTitleClick.bind(this);
    this.__onAnimationEnd = this._onAnimationEnd.bind(this);
    return self;
  }

  /**
   *
   */
  connectedCallback() {
    this.shadowRoot.innerHTML = `
        <style>` + this._css + `</style>
        <div class="accordion-panel accordion-panel__closed">
          <div class="accordion-panel-title">
            <div class="accordion-panel-title-button">
              <svg class="accordion-panel-title-button-icon" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                <path d="M1408 704q0 26-19 45l-448
                448q-19 19-45 19t-45-19l-448-448q-19-19-19-45t19-45
                45-19h896q26 0 45 19t19 45z"/>
              </svg>
            </div><div class="accordion-panel-title-text">
              <slot name="title" id="title-slot"></slot>
            </div>
          </div>
          <div class="accordion-panel-body">
            <slot name="body" id="body-slot"></slot>
          </div>
        </div>`;

    this.shadowRoot.querySelector('.accordion-panel-title')
      .addEventListener('click', this.__onTitleClick);
    this.shadowRoot.querySelector('.accordion-panel')
      .addEventListener('animationend', this.__onAnimationEnd, false);
  }

  /**
   *
   */
  disconnectedCallback() {
    this.shadowRoot.querySelector('.accordion-panel-title')
      .removeEventListener('click', this.__onTitleClick);
    this.shadowRoot.querySelector('.accordion-panel')
      .removeEventListener('animationend', this.__onAnimationEnd, false);
  }

  /**
   * @param {e} e event
   */
  _onAnimationEnd(e) {
    if(e.animationName === 'accordion-in') {
      this.shadowRoot.querySelector('.accordion-panel')
        .classList.remove('accordion-panel__animate-in');
    } else if(e.animationName === 'accordion-out') {
      this.shadowRoot.querySelector('.accordion-panel')
        .classList.remove('accordion-panel__animate-out');
    }
  }

  /**
   * @param {e} e event
   */
  _onTitleClick(e) {
    const pnl = this.shadowRoot.querySelector('.accordion-panel');
    if(this.shadowRoot.querySelector('.accordion-panel')
      .classList.contains('accordion-panel__closed')) {
        this.shadowRoot.querySelector('.accordion-panel')
          .classList.add('accordion-panel__animate-in');
        this.shadowRoot.querySelector('.accordion-panel')
          .classList.remove('accordion-panel__closed');
      if(this._clearAnimTimeout){
        clearTimeout(this._clearAnimTimeout);
        this._clearAnimTimeout = null;
      }
      this._clearAnimTimeout = setTimeout(function(){
        pnl.classList.remove('accordion-panel__animate-in');
      }, 360)

    } else {
      this.shadowRoot.querySelector('.accordion-panel')
        .classList.add('accordion-panel__closed');
      this.shadowRoot.querySelector('.accordion-panel')
        .classList.add('accordion-panel__animate-out');
      if(this._clearAnimTimeout){
        clearTimeout(this._clearAnimTimeout);
        this._clearAnimTimeout = null;
      }
      this._clearAnimTimeout = setTimeout(function(){
        pnl.classList.remove('accordion-panel__animate-out');
      }, 360)
    }

  }
}

customElements.define('p-accordion-panel', PAccordionPanel);
