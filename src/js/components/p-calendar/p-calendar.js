import css from './p-calendar.css';

/**
 *
 */
export class PCalendar extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this._css = css;
    this.attachShadow({mode: 'open'});
    this.__onClick = this._onClick.bind(this);
    this.__onPrevNextClick = this._onPrevNextClick.bind(this);
    this.__onYearInputChange = this._onYearInputChange.bind(this);
    this.__onYearInputFocus = this._onYearInputFocus.bind(this);
    this.__onYearInputInput = this._onYearInputInput.bind(this);
    this.__onMonthSelectChange = this._onMonthSelectChange.bind(this);
    this.monthLabels = ['januari', 'februari', 'maart', 'april',
      'mei', 'juni', 'juli', 'augustus', 'september',
      'oktober', 'november', 'december',
    ];
    this.dayLabels = ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'];
    this.previousLabel = 'vorige';
    this.nextLabel = 'volgende';

    return self;
  }

  /**
   *
   */
  get month() {
    return this._month;
  }

  /**
   * @param {value} value
   */
  set month(value) {
    /**
     * @param {message} message
     */
    function UserException(message) {
      this.message = message;
      this.name = 'UserException';
    }
    value = parseInt(value);
    if (value < 0) {
      throw new UserException('InvalidMonthNo');
    } else if (value > 11) {
      throw new UserException('InvalidMonthNo');
    }
    this._month = parseInt(value);
    this._renderCalendar();
  }

  /**
   *
   */
  get year() {
    return this._year;
  }

  /**
   * @param {value} value
   */
  set year(value) {
    this._year = parseInt(value);
    this._renderCalendar();
  }

  /**
   *
   */
  connectedCallback() {
    this.shadowRoot.innerHTML = '<style>' + this._css + '<style>';
    this._headerElement = document.createElement('header');
    this._headerElement.classList.add('calendar-header');
    this.shadowRoot.appendChild(this._headerElement);
    this._prevMonthElement = document.createElement('a');
    this._prevMonthElement.classList.add('previous-month');
    this._prevMonthElement.innerText = this.previousLabel;
    this._prevMonthElement.setAttribute('href', '#');
    this._prevMonthElement.setAttribute('onclick', 'return false;');
    this._prevMonthElement.addEventListener('click', this.__onPrevNextClick);
    this._headerElement.appendChild(this._prevMonthElement);
    this._nextMonthElement = document.createElement('a');
    this._nextMonthElement.classList.add('next-month');
    this._nextMonthElement.innerText = this.nextLabel;
    this._nextMonthElement.setAttribute('href', '#');
    this._nextMonthElement.setAttribute('onclick', 'return false;');
    this._nextMonthElement.addEventListener('click', this.__onPrevNextClick);
    this._headerElement.appendChild(this._nextMonthElement);
    this._titleElement = document.createElement('div');
    this._headerElement.appendChild(this._titleElement);
    this._titleMonthElement = document.createElement('select');
    this._titleMonthElement.classList.add('month-title');
    for (let i = 0; i <= 11; i++) {
      let monthOption = document.createElement('option');
      monthOption.value = i;
      monthOption.innerText = this.monthLabels[i];
      this._titleMonthElement.appendChild(monthOption);
    }
    this._titleElement.appendChild(this._titleMonthElement);
    this._titleMonthElement.addEventListener('change',
      this.__onMonthSelectChange);
    this._titleYearElement = document.createElement('input');
    this._titleYearElement.setAttribute('type', 'number');
    this._titleYearElement.classList.add('year-title');
    this._titleElement.appendChild(this._titleYearElement);
    this._titleYearElement.addEventListener('change', this.__onYearInputChange);
    this._titleYearElement.addEventListener('focus', this.__onYearInputFocus);
    this._titleYearElement.addEventListener('input', this.__onYearInputInput);
    this._bodyElement = document.createElement('div');
    this.shadowRoot.appendChild(this._bodyElement);
    this._bodyElement.addEventListener('click', this.__onClick);
    this._footerElement = document.createElement('footer');
    this.shadowRoot.appendChild(this._footerElement);
    this._renderCalendar();
  }

  /**
   *
   */
  disconnectedCallback() {
    this._prevMonthElement.removeEventListener('click', this.__onPrevNextClick);
    this._nextMonthElement.removeEventListener('click', this.__onPrevNextClick);
    this._titleMonthElement.removeEventListener('change',
      this.__onMonthSelectChange);
    this._titleYearElement.removeEventListener('change',
      this.__onYearInputChange);
    this._titleYearElement.removeEventListener('focus',
      this.__onYearInputFocus);
    this._titleYearElement.removeEventListener('input',
      this.__onYearInputInput);
  }

  /**
   *
   */
  static get observedAttributes() {
    return ['month', 'year'];
  }

  /**
   * @param {name} name
   * @param {oldValue} oldValue
   * @param {newValue} newValue
   */
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'month') {
      this.month = newValue;
    } else if (name === 'year') {
      this.year = newValue;
    }
  }

  /**
   * @param {e} e event
   */
  _onMonthSelectChange(e) {
    this.month = e.target.options[e.target.selectedIndex].value;
  }

  /**
   * @param {e} e event
   */
  _onYearInputInput(e) {
    if (('' + e.target.value).length === 4) {
      document.activeElement.blur();
      e.target.value = e.target.value.slice(0, 4);
    }
  }

  /**
   * @param {e} e event
   */
  _onYearInputChange(e) {
    this.year = e.target.value;
  }

  /**
   * @param {e} e event
   */
  _onYearInputFocus(e) {
    e.target.select();
  }

  /**
   * @param {e} e event
   */
  _onPrevNextClick(e) {
    if (this.year !== null && this.month !== null) {
      let newMonth;
      if (e.target.classList.contains('previous-month')) {
        newMonth = new Date(this.year, this.month - 1, 1);
      } else {
        newMonth = new Date(this.year, this.month + 1, 1);
      }
      this.year = newMonth.getFullYear();
      this.month = newMonth.getMonth();
    }
  }

  /**
   * @param {e} e event
   */
  _onClick(e) {
    if (e.target.classList.contains('calendar-day--link')) {
      this.dispatchEvent(new CustomEvent('date-click', {
        bubbles: true,
        composed: true,
        detail: this.year + '-' + (this.month + 1) + '-' + e.target.innerText,
      }));
    }
  }

  /**
   *
   */
  _renderCalendar() {
    if (typeof this.year === 'undefined') {
      this.year = new Date().getFullYear();
    }
    if (typeof this.month === 'undefined') {
      this.month = new Date().getMonth();
    }
    let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    let firstDay = new Date(this.year, this.month, 1);
    let startingDay = firstDay.getDay();
    let monthLength = daysInMonth[this.month];

    // compensate for leap year
    if (this.month === 1) {
      if ((this.year % 4 === 0 && this.year % 100 !== 0) ||
        this.year % 400 === 0) {
          monthLength = 29;
      }
    }
    this._titleYearElement.value = this.year;
    this._titleMonthElement.value = this.month;

    let html = '<table class="calendar-table">';
    html += '<tr class="calendar-header">';
    for (let i = 0; i <= 6; i++) {
      html += '<td class="calendar-header-day">';
      html += this.dayLabels[i];
      html += '</td>';
    }
    html += '</tr><tr>';
    let day = 1;
    for (let ii = 0; ii < 9; ii++) {
      for (let j = 0; j <= 6; j++) {
        html += '<td class="calendar-day">';
        if (day <= monthLength && (ii > 0 || j >= startingDay)) {
          html += '<a href="#" onclick="return false;" ' +
            'class="calendar-day--link">';
          html += day;
          html += '</a>';
          day++;
        }
        html += '</td>';
      }
      if (day > monthLength) {
        break;
      } else {
        html += '</tr><tr>';
      }
    }
    html += '</tr></table>';
    this._bodyElement.innerHTML = html;
  }

}

customElements.define('p-calendar', PCalendar);
