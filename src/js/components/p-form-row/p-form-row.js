import css from './p-form-row.css';

/**
 *
 */
export class PFormRow extends HTMLElement {

  /**
   *
   */
  constructor() {
    let self = super();
    this._css =css;
    this.attachShadow({mode: 'open'});
    return self;
  }

  /**
   *
   */
  connectedCallback() {
    this.shadowRoot.innerHTML = `<style>${this._css}</style>` +
      '<div class="form-row--legend"><slot name="legend"></slot></div>' +
      '<div class="form-row--label"><slot name="label"></slot></div>' +
      '<div class="form-row--field"><slot name="field"></slot></div>';
  }
}

customElements.define('p-form-row', PFormRow);
