import cssPicker from './date-picker-panel.css';
import css from './p-input-date.css';

/**
 *
 */
export class PInputDate extends HTMLElement {

  /**
   *
   */
  constructor() {
    let self = super();
    this._cssPicker = cssPicker;
    this._css = css;
    this.attachShadow({mode: 'open'});
    this._format = 'yyyy-mm-dd';
    this._placeholder = '';
    this.__onDatePickerButtonClick = this._onDatePickerButtonClick.bind(this);
    this.__onWindowDocumentClick = this._onWindowDocumentClick.bind(this);
    this.__onWindowScroll = this._onWindowScroll.bind(this);
    this.__onInputChange = this._onInputChange.bind(this);
    this.__onCalendarDateClick = this._onCalendarDateClick.bind(this);

    if (!document.getElementById('style-date-picker-panel')) {
      let styleElem = document.createElement('style');
      styleElem.setAttribute('id', 'style-date-picker-panel');
      styleElem.innerHTML = this._cssPicker;
      document.getElementsByTagName('head')[0].appendChild(styleElem);
    }
    return self;
  }

  /**
   * @param {e} e event
   */
  _onInputChange(e) {
    let dateVal = this._parseDate(e.target.value);
    if (dateVal) {
      this.value = dateVal.getFullYear() +
        '-' + (dateVal.getMonth() + 1) + '-' + dateVal.getDate();
    } else {
      this.value = '';
    }
  }

  /**
   * @param {e} e event
   */
  _onCalendarDateClick(e) {
    this.value = e.detail;
    this._hideDatePicker();
  }

  /**
   *
   */
  static get observedAttributes() {
    return ['format', 'value', 'name', 'placeholder'];
  }

  /**
   * @param {value} value
   */
  set placeholder(value) {
    this._placeholder = value;
    if (this.dateInput) {
      this.dateInput.placeholder = value;
    }
  }

  /**
   *
   */
  get placeholder() {
    return this._placeholder;
  }

  /**
   * @param {value} value
   */
  set name(value) {
    this._name = value;
  }

  /**
   *
   */
  get name() {
    return this._name;
  }

  /**
   * @param {value} value
   */
  set format(value) {
    this._format = value;
  }

  /**
   *
   */
  get format() {
    return this._format;
  }

  /**
   * @param {value} value
   */
  set value(value) {
    let dateVal = null;
    if (value) {
      dateVal = this._parseDate(value, 'yyyy-mm-dd');
    }

    if (dateVal) {
      this._value = dateVal.getFullYear() + '-' +
        (dateVal.getMonth() + 1) + '-' + dateVal.getDate();
      let val = this.format;
      val = val.replace('yyyy', dateVal.getFullYear());
      val = val.replace('mm', dateVal.getMonth() + 1);
      val = val.replace('dd', dateVal.getDate());

      if (this.dateInput) {
        this.dateInput.value = val;
      }
    } else {
      this._value = '';
    }
  }

  /**
   *
   */
  get value() {
    return this._value;
  }

  /**
   *
   */
  connectedCallback() {
    this.shadowRoot.innerHTML = `
      <style>${this._css}</style>
      <input id="DateInput" class="input-date" type="text" tabindex="0" />
      <a tabindex="0" id="DatePickerButton" class="date-picker-button">
        <svg class="date-picker-icon-svg" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
          <path d="M1408 704q0 26-19
          45l-448 448q-19 19-45 19t-45-19l-448-448q-19-19-19-45t19-45
          45-19h896q26 0 45 19t19 45z"/>
        </svg>
      </a>`;
    this.datePickerButton = this.shadowRoot.getElementById('DatePickerButton');
    this.dateInput = this.shadowRoot.getElementById('DateInput');

    this.value = this._value;
    this.format = this._format;
    this.dateInput.placeholder = this._placeholder;

    this.datePickerButton.addEventListener('touchstart',
      this.__onDatePickerButtonClick);
    this.datePickerButton.addEventListener('click',
      this.__onDatePickerButtonClick);
    this.dateInput.addEventListener('change', this.__onInputChange);
  }

  /**
   *
   */
  disconnectedCallback() {
    this.datePickerButton.removeEventListener('touchstart',
      this.__onDatePickerButtonClick);
    this.datePickerButton.removeEventListener('click',
      this.__onDatePickerButtonClick);
  }

  /**
   * @param {name} name
   * @param {oldValue} oldValue
   * @param {newValue} newValue
   */
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'format') {
      this.format = newValue;
    } else if (name === 'value') {
      this.value = newValue;
    } else if (name === 'name') {
      this.name = newValue;
    } else if (name === 'placeholder') {
      this.placeholder = newValue;
    }
  }

  /**
   *
   */
  focus() {
    this.dateInput.focus();
  }

  /**
   * @param {e} e event
   */
  _onDatePickerButtonClick(e) {
    this._showDatePicker();
  }

  /**
   * @param {e} e event
   */
  _onWindowDocumentClick(e) {
    if(!e) {
      e = window.event;
    }
    if (!e.target.closest('p-input-date')) {
      if (!e.target.closest('.date-picker-panel')) {
        this._hideDatePicker();
      }
    }
  }

  /**
   * @param {e} e event
   */
  _onWindowScroll(e) {
    this._positionDatePickerPanel();
  }

  /**
   * @param {dateString} dateString
   * @param {format} format
   * @return {Date}
   */
  _parseDate(dateString, format) {
    let daysInMonth = function(month, year) {
      return new Date(year, month, 0).getDate();
    };
    if (!format) {
      format = this._format;
    }
    let parts = dateString.match(/(\d+)/g);
    let i = 0;
    let fmt = {};
    format.replace(/(yyyy|dd|mm)/g, function(part) {
      fmt[part] = i++;
    });

    if (!parts || parts.length < 3) {
      return null;
    }
    if (parts[fmt['mm']] - 1 > 11 || parts[fmt['mm']] - 1 < 0) {
      return null;
    }
    if (parts[fmt['dd']] > daysInMonth(parts[fmt['mm']], parts[fmt['yyyy']])) {
      return null;
    }
    return new Date(parts[fmt['yyyy']], parts[fmt['mm']] - 1, parts[fmt['dd']]);
  }

  /**
   *
   */
  _showDatePicker() {
    let pickerDate = this._parseDate(this.value, 'yyyy-mm-dd');
    if (!this._datePicker) {
      this._datePicker = document.createElement('div');
      this._datePicker.classList.add('date-picker-panel');
      this._datePicker.setAttribute('tabindex', '0');
      this._datePickerCalendar = document.createElement('p-calendar');
      this._datePicker.appendChild(this._datePickerCalendar);

      document.body.appendChild(this._datePicker);
    }
    if (pickerDate) {
      this._datePickerCalendar.year = pickerDate.getFullYear();
      this._datePickerCalendar.month = pickerDate.getMonth();
    }
    this._datePicker.style.display = 'block';

    this._positionDatePickerPanel();
    this._datePicker.focus();

    this._datePicker.addEventListener('date-click', this.__onCalendarDateClick);
    document.body.addEventListener('touchstart',
      this.__onWindowDocumentClick, {passive: true});
    window.document.addEventListener('click',
      this.__onWindowDocumentClick, {passive: true});
    window.addEventListener('scroll', this.__onWindowScroll, {passive: true});
  }

  /**
   *
   */
  _positionDatePickerPanel() {
    let leftPos = this.getBoundingClientRect().left;
    let topPos = this.getBoundingClientRect().top;
    let elemHeight = this.clientHeight;
    this._datePicker.style.left = leftPos + 'px';
    this._datePicker.style.top = topPos + elemHeight + 'px';
    this._datePicker.style.position = 'fixed';
  }

  /**
   *
   */
  _hideDatePicker() {
    this._datePicker.removeEventListener('date-click',
      this.__onCalendarDateClick);
    window.document.removeEventListener('click',
      this.__onWindowDocumentClick);
    document.body.removeEventListener('touchstart',
      this.__onWindowDocumentClick);
    window.removeEventListener('scroll', this.__onWindowScroll);
    this._datePicker.style.display = 'none';
    this.dateInput.focus();
  }

}

customElements.define('p-input-date', PInputDate);
