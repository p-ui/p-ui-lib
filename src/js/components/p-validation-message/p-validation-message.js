import css from './p-validation-message.css';

/**
 *
 */
export class PValidationMessage extends HTMLElement {

  /**
   * @param {self} self
   */
  constructor(self) {
    self = super(self);
    this._css = css;
    this.attachShadow({mode: 'open'});
    this.__onElementInvalid = this._onElementInvalid.bind(this);
    this.__onElementBlur = this._onElementBlur.bind(this);
    return self;
  }

  /**

   */
  get validationType() {
    if (!this._validationType) {
      return '';
    }
    return this._validationType;
  }

  /**
   * @param {validationType} validationType
   */
  set validationType(validationType) {
    this._validationType = validationType;
  }

  /**
   *
   */
  get validationFor() {
    return this._validationFor;
  }

  /**
   * @param {validationFor} validationFor
   */
  set validationFor(validationFor) {
    let elementFor;
    this.setAttribute('hidden', '');
    if (this._validationFor) {
      elementFor = document.getElementById(this.validationFor);
      if (elementFor) {
        elementFor.removeEventListener('invalid', this.__onElementInvalid);
        elementFor.removeEventListener('blur', this.__onElementBlur);
      }
    }

    this._validationFor = validationFor;
    elementFor = document.getElementById(this.validationFor);
    if (elementFor) {
      elementFor.addEventListener('invalid', this.__onElementInvalid);
      elementFor.addEventListener('blur', this.__onElementBlur);
    }
  }

  /**
   *
   */
  static get observedAttributes() {
    return ['validation-type', 'validation-for'];
  }

  /**
   * @param {name} name
   * @param {oldValue} oldValue
   * @param {newValue} newValue
   */
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'validation-for') {
      this.validationFor = newValue;
    } else if (name === 'validation-type') {
      this.validationType = newValue;
    }
  }

  /**
   *
   */
  connectedCallback() {
    this.shadowRoot.innerHTML = `
      <style>${this._css}</style><slot></slot>`;
    this.validationFor = this.getAttribute('validation-for');
  }

  /**
   *
   */
  disconnectedCallback() {
    if (this._validationFor) {
      let elementFor = document.getElementById(this.validationFor);
      if (elementFor) {
        elementFor.removeEventListener('invalid', this.__onElementInvalid);
        elementFor.removeEventListener('blur', this.__onElementBlur);
      }
    }
  }

  /**
   * @param {e} e event
   */
  _onElementInvalid(e) {
    let elem = document.getElementById(this.validationFor);
    if (elem === e.target) {
      let types = this.validationType.split(' ');
      this.setAttribute('hidden', '');

      if (types.indexOf('required') > -1 && elem.validity.valueMissing) {
        this.removeAttribute('hidden');
      }

      if (types.indexOf('pattern') > -1 && elem.validity.patternMismatch) {
        this.removeAttribute('hidden');
      }

      if (types.indexOf('custom') > -1 && elem.validity.customError) {
        // this.bindings.validationMessage = elem.validationMessage;
        this.removeAttribute('hidden');
      }

      if (types.indexOf('type') > -1 && elem.validity.typeMismatch) {
        this.removeAttribute('hidden');
      }

      if (types.indexOf('overflow') > -1 && elem.validity.rangeOverflow) {
        this.removeAttribute('hidden');
      }

      if (types.indexOf('underflow') > -1 && elem.validity.rangeUnderflow) {
        this.removeAttribute('hidden');
      }
    }
  }

  /**
   * @param {e} e event
   */
  _onElementBlur(e) {
    if (e.target.id !== this.validationFor) {
      return;
    }
    this.setAttribute('hidden', '');
    e.target.checkValidity();
  }

}

customElements.define('p-validation-message', PValidationMessage);
