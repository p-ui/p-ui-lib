import {PDialog} from './components/p-dialog/p-dialog';
import {PAccordion} from './components/p-accordion/p-accordion';
import {PAccordionPanel} from './components/p-accordion/p-accordion-panel';
import {PForm} from './components/p-form/p-form';
import {PFormRow} from './components/p-form-row/p-form-row';
import {PLabel} from './components/p-label/p-label';
import {PCalendar} from './components/p-calendar/p-calendar';
import {PInputDate} from './components/p-input-date/p-input-date';
import {PInformationIcon}
  from './components/p-information-icon/p-information-icon';
import {PTooltip} from './components/p-tooltip/p-tooltip';
import {PValidationMessage}
  from './components/p-validation-message/p-validation-message';

window.PDialog = PDialog;
window.PAccordion = PAccordion;
window.PAccordionPanel = PAccordionPanel;
window.PForm = PForm;
window.PFormRow = PFormRow;
window.PLabel = PLabel;
window.PCalendar = PCalendar;
window.PInputDate = PInputDate;
window.PInformationIcon = PInformationIcon;
window.PTooltip = PTooltip;
window.PValidationMessage = PValidationMessage;
