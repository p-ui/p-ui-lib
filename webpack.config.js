'use strict';

module.exports = {
     entry:
     {
       'p-ui-lib': './src/js/main.js'
     },
     output: {
         path: 'dist/js',
         filename: '[name].js'
     },
     module: {
         loaders: [
           {
             test: /\.js$/,
             exclude: /node_modules/,
             loader: 'babel-loader'
          },
          {
            test: /\.css$/,
            loaders: [
              'to-string-loader',
              'css-loader',
              'postcss-loader'
            ]
          }
        ]
     },
     resolve: {
        extensions: ['', '.js', '.css']
     },
     postcss: () => {
      return [
        require('postcss-smart-import')({}),
        require('postcss-cssnext')({warnForDuplicates: false}),
        //require('cssnano')({})
      ];
    },
    devtool: 'source-map'
 };
