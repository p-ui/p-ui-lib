module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      {
        pattern: './dist/js/**/*.*',
        included: false,
        watched: false,
        served: true,
      },
      'test/scriptloader.js',
      'test/components/p-accordion-test.js',
    ],
    reporters: [
      'dots',
    ],
    port: 9876,
    runnerPort: 9100,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: true,
  });
};
