(function(window) {
  window.__karma__.loaded = function() { };

  let supportsCustomElementsV1 = 'customElements' in window;

  /**
   * @param {string} src script file
   * @param {function} done callback function
   */
  function loadScript(src, done) {
    let js = document.createElement('script');
    js.src = src;
    js.onload = function() {
      done();
    };
    js.onerror = function() {
      done(new Error('Failed to load script ' + src));
    };
    document.head.appendChild(js);
  }

  if (supportsCustomElementsV1) {
    loadScript('base/dist/js/native-shim.min.js',
      function() {
        loadScript('base/dist/js/p-ui-lib.min.js', function() {
          window.__karma__.start();
        });
      }
    );
  } else {
    loadScript('base/dist/js/shadow-dom.min.js',
      function(e) {
        loadScript('base/dist/js/p-ui-lib.min.js', function() {
          window.__karma__.start();
        });
      });
  }
}(window));
