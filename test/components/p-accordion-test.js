'use strict';

/**
 *
 */
describe('PAccordion', function () {

  var accordionElement;

  /**
   *
   */
  beforeEach(function () {
    accordionElement = document.createElement('p-accordion');
  });

  /**
   *
   */
  it('p-accordion element should match string representation', function () {
    var expectedEl = '<p-accordion></p-accordion>';
    var wrapper = document.createElement('div');
    wrapper.appendChild(accordionElement);
    document.body.appendChild(wrapper);
    debugger;
    expect(wrapper.innerHTML).toBe(expectedEl);
    //wrapper.parentNode.removeChild(wrapper);
  });

  /**
   *
   */
  it('p-accordion element should have shadow root with a style and slot element', function () {
    var wrapper = document.createElement('div');
    wrapper.appendChild(accordionElement);
    document.body.appendChild(wrapper);
    var shadowRoot = wrapper.querySelector('p-accordion').shadowRoot;
    var slot = shadowRoot.querySelector('slot');;
    var style = shadowRoot.querySelector('style');;

    expect(shadowRoot).not.toBeNull();
    expect(slot).not.toBeNull();
    expect(style).not.toBeNull();

    //wrapper.parentNode.removeChild(wrapper);
  });

  /**
   *
   */
  it('p-accordion-panel element should match string representation', function () {
    var panelHTML = '<div slot="title">accordion title</div><div slot="body">accordion body</div>';
    var expectedEl = '<p-accordion-panel>'+panelHTML+'</p-accordion-panel>';
    var wrapper = document.createElement('div');
    var accordionPanelElement = document.createElement('p-accordion-panel');
    accordionPanelElement.innerHTML = panelHTML;
    accordionElement.appendChild(accordionPanelElement);
    wrapper.appendChild(accordionElement);
    document.body.appendChild(wrapper);
    expect(accordionElement.innerHTML).toBe(expectedEl);
   //wrapper.parentNode.removeChild(wrapper);
  });

  /**
   *
   */
  it('p-accordion-panel element should have shadow root with a style and 2 slot elements', function () {
    var panelHTML = '<div slot="title">accordion title</div><div slot="body">accordion body</div>';
    var wrapper = document.createElement('div');
    var accordionPanelElement = document.createElement('p-accordion-panel');
    wrapper.appendChild(accordionElement);
    accordionPanelElement.innerHTML = panelHTML;
    accordionElement.appendChild(accordionPanelElement);
    document.body.appendChild(wrapper);

    var shadowRoot = accordionElement.querySelector('p-accordion-panel').shadowRoot;
    var titleSlot = shadowRoot.querySelectorAll('slot[name="title"]');
    var bodySlot = shadowRoot.querySelectorAll('slot[name="body"]');
    var style = shadowRoot.querySelector('style');;

    expect(shadowRoot).not.toBeNull();
    expect(titleSlot).not.toBeNull();
    expect(bodySlot).not.toBeNull();
    expect(style).not.toBeNull();
    //wrapper.parentNode.removeChild(wrapper);
  });

  /**
   *
   */
  it('click on title element of accordion panel should remove class from body', function () {
    var panelHTML = '<div slot="title">accordion title</div><div slot="body">accordion body</div>';
    var wrapper = document.createElement('div');
    var accordionPanelElement = document.createElement('p-accordion-panel');
    wrapper.appendChild(accordionElement);
    accordionPanelElement.innerHTML = panelHTML;
    accordionElement.appendChild(accordionPanelElement);
    document.body.appendChild(wrapper);

    var shadowRoot = accordionElement.querySelector('p-accordion-panel').shadowRoot;
    var accordionPanel = shadowRoot.querySelector('.accordion-panel');
    var accordionPanelTitle = shadowRoot.querySelector('.accordion-panel-title');

    expect(accordionPanel.classList.contains('accordion-panel__closed')).toBeTruthy();
    accordionPanelTitle.click();
    expect(accordionPanel.classList.contains('accordion-panel__closed')).toBeFalsy();
    //wrapper.parentNode.removeChild(wrapper);
  });

});
